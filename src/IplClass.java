import java.io.*;
import java.util.*;

public class IplClass {

    public void matchesPerYear(List<String> matchesData){

        Map<String,Integer> matchesPerYearData = new HashMap<>();
        for(String lineData: matchesData ){

            String []matchData=lineData.split(",");
            matchesPerYearData.put(matchData[1],matchesPerYearData.getOrDefault(matchData[1],0)+1);
//                if(matchesPerYearData.containsKey(matchData[1])){
//                    matchesPerYearData.put(matchData[1],matchesPerYearData.get(matchData[1])+1);
//                }
//                else{
//                    matchesPerYearData.put(matchData[1],1);
//                }
        }
        System.out.println(matchesPerYearData);

    }


    public void matchesWonPerTeam(List<String> matchesData){

        Map<String,Integer> matchesWonPerTeamData= new HashMap<>();

        for(String lineData: matchesData ){
            String []matchData=lineData.split(",");
            String teamWon = matchData[10];
            if(!teamWon.equals("")) {
                matchesWonPerTeamData.put(teamWon,matchesWonPerTeamData.getOrDefault(teamWon,0)+1);
            }
        }
        System.out.println(matchesWonPerTeamData);
    }


    public void extraRunPerTeam2016(List<String> matchesData,List<String> deliveriesData) {

        Map<String,Integer> extraRunPerTeam = new HashMap<String,Integer>();
        for(String lineData: matchesData ){
            String []matchData=lineData.split(",");
            String season=matchData[1];
            if(season.equals("2016")) {
                String matchId=matchData[0];
                for(String deliveryLineData : deliveriesData ){
                    String []deliveryData = deliveryLineData.split(",");
                    if(matchId.equals(deliveryData[0])){
                        extraRunPerTeam.put(deliveryData[3],extraRunPerTeam.getOrDefault(deliveryData[3],0)+Integer.parseInt(deliveryData[16]));
                    }
                }
            }
        }
        System.out.println(extraRunPerTeam);
    }


    public void topEconomicalBowlers2015(List<String> matchesData, List<String> deliveriesData) {
        Map<String,List<Integer>>  topEconomicalBowlerData = new HashMap<String,List<Integer>>();
        Map<String,Float> topEconomyBowler = new HashMap<String,Float>();

        for(String matchLineData : matchesData){
            String []matchData = matchLineData.split(",");
            if(matchData[1].equals("2015")){
                for(String deliveryLineData : deliveriesData){
                    String []deliveryData = deliveryLineData.split(",");
                    if(matchData[0].equals(deliveryData[0])){
                        List<Integer> bowler =new ArrayList<Integer>();
                        if(topEconomicalBowlerData.containsKey(deliveryData[8])){
                            bowler.add(topEconomicalBowlerData.get(deliveryData[8]).get(0)+Integer.parseInt(deliveryData[17]));
                            bowler.add(topEconomicalBowlerData.get(deliveryData[8]).get(1)+1);
                            topEconomicalBowlerData.put(deliveryData[8],bowler);
                        }
                        else {
                            bowler.add(Integer.parseInt(deliveryData[17]));
                            bowler.add(1);
                            topEconomicalBowlerData.put(deliveryData[8],bowler);
                        }
                    }
                }
            }
        }

        for(Map.Entry bowlersData : topEconomicalBowlerData.entrySet()){
            List<Integer> runNball = new ArrayList<>();
            runNball = (List<Integer>) bowlersData.getValue();
            topEconomyBowler.put((String) bowlersData.getKey(),runNball.get(0)/(runNball.get(1)/6f));
        }

        Set<Map.Entry<String,Float>> entrySet = topEconomyBowler.entrySet();
        List<Map.Entry<String,Float>> bowlerList = new ArrayList<>(entrySet);
        bowlerList.sort(new Comparator<Map.Entry<String, Float>>() {
            @Override
            public int compare(Map.Entry<String, Float> o1, Map.Entry<String, Float> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });

        System.out.println(bowlerList);
    }


    public void wonTossAndMatch(List<String> matchesData) {
        Map<String,Integer> wonTossAndMatchData = new HashMap<String,Integer>();
        for(String lineData: matchesData ) {

            String[] matchData = lineData.split(",");
            if(matchData[6].equals(matchData[10])){
                wonTossAndMatchData.put(matchData[6],wonTossAndMatchData.getOrDefault(matchData[6],0)+1);
            }
        }
        System.out.println(wonTossAndMatchData);
    }

    public void manOfTheMatches(List<String> matchesData){

        Map<String,Integer> manOfTheMatchesData = new HashMap<String,Integer>();
        for(String lineData: matchesData){
            String []matchData = lineData.split(",");
            if(!matchData[13].equals("")) {
                manOfTheMatchesData.put(matchData[13], manOfTheMatchesData.getOrDefault(matchData[13], 0) + 1);
            }
        }
        System.out.println(manOfTheMatchesData);
    }


    public void viratKohliRun2016(List<String> matchesData, List<String> deliveriesData) {
        int viratKohliRun = 0;
        for(String matchLineData : matchesData) {
            String []matchData = matchLineData.split(",");
            if(matchData[1].equals("2016")) {
                for(String deliveryLineData : deliveriesData ){
                    String []deliveryData = deliveryLineData.split(",");
                    if(matchData[0].equals(deliveryData[0])&&deliveryData[6].equals("V Kohli")){
                        viratKohliRun += Integer.parseInt(deliveryData[15]);
                    }
                }
            }
        }
        System.out.println("\nVirat Kohli Run in 2016 \n"+viratKohliRun);
    }


    public void bumrahOutKohli(List<String> deliveriesData) {
        int out=0;
        for(String deliveryLineData : deliveriesData ){
            String []deliveryData = deliveryLineData.split(",");
            if(deliveryData.length>18 && deliveryData[8].equals("JJ Bumrah") && deliveryData[6].equals("V Kohli") && !deliveryData[19].equals("run out")){
                out+=1;
            }
        }
        System.out.println("\nJJ Bumrah Out Virat Kohli "+out+" Times");
    }


    public void matchesTieTeamCount(List<String> matchesData) {
        Map<String,Integer> teamsMatchesTie = new HashMap<String,Integer>();
        for(String matchLineData: matchesData){
            String []matchData = matchLineData.split(",");
            if(matchData[8].equals("tie")) {
                teamsMatchesTie.put(matchData[4],teamsMatchesTie.getOrDefault(matchData[4],0)+1);
                teamsMatchesTie.put(matchData[5],teamsMatchesTie.getOrDefault(matchData[5],0)+1);
            }
        }
        System.out.println(teamsMatchesTie);
    }


    public void totalRunsScoreByTeamIn2016(List<String> matchesData, List<String> deliveriesData) {
        Map<String,Integer> totalRunByTeam = new HashMap<>();
        for(String matchLineData: matchesData) {
            String []matchData = matchLineData.split(",");
            if(matchData[1].equals("2016")){
                for(String deliveryLineData : deliveriesData) {
                    String []deliveryData = deliveryLineData.split(",");
                    if(matchData[0].equals(deliveryData[0])) {
                        totalRunByTeam.put(deliveryData[2],totalRunByTeam.getOrDefault(deliveryData[2],0)+1);
                    }
                }
            }
        }
        System.out.println(totalRunByTeam);
    }
    public static void main(String []args) throws IOException {

        String deliveriesFilePath = "./src/data/deliveries.csv";
        String matchesFilePath = "./src/data/matches.csv";
        List<String> deliveriesFileData = new ArrayList<String>();
        List<String> matchesFileData = new ArrayList<String>();
        IplClass iplObject=new IplClass();
        String lineData="";
        try {
            BufferedReader deliveriesData = new BufferedReader(new FileReader(deliveriesFilePath));
            BufferedReader matchesData = new BufferedReader(new FileReader(matchesFilePath));

            matchesData.readLine();
            while ((lineData = matchesData.readLine()) != null) {
                matchesFileData.add(lineData);
            }

            deliveriesData.readLine();
            while((lineData = deliveriesData.readLine()) != null) {
                deliveriesFileData.add(lineData);
            }

            System.out.println("Number of matches played per year of all the years in IPL");
            iplObject.matchesPerYear(matchesFileData);
            System.out.println("\nNumber of matches won of all teams over all the years of IPL.");
            iplObject.matchesWonPerTeam(matchesFileData);
            System.out.println("\nFor the year 2016 get the extra runs conceded per team.");
            iplObject.extraRunPerTeam2016(matchesFileData,deliveriesFileData);
            System.out.println("\nFor the year 2015 get the top economical bowlers.");
            iplObject.topEconomicalBowlers2015(matchesFileData,deliveriesFileData);
            System.out.println("\nTeam Won the match and won match also");
            iplObject.wonTossAndMatch(matchesFileData);
            System.out.println("\nNO of man of the matches players won");
            iplObject.manOfTheMatches(matchesFileData);
            iplObject.viratKohliRun2016(matchesFileData,deliveriesFileData);
            iplObject.bumrahOutKohli(deliveriesFileData);
            System.out.println("\nNo of time matches tie per team");
            iplObject.matchesTieTeamCount(matchesFileData);
            System.out.println("\nTotal run Score by a Teams in 2016");
            iplObject.totalRunsScoreByTeamIn2016(matchesFileData,deliveriesFileData);

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }


    }

}