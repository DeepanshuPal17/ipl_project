package com.iplproject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {

    private final int MATCHID = 0;
    private final int SEASON = 1;
    private final int TEAM1 = 4;
    private final int TEAM2 = 5;
    private final int TOSSWINNER = 6;
    private final int RESULT = 8;
    private final int WINNERTEAM = 10;
    private final int PLAYEROFMATCH = 13;
    private final int BATTINGTEAM = 2;
    private final int BOWLINGTEAM = 3;
    private final int BATSMEN = 6;
    private final int BOWLING = 8;
    private final int BATSMENRUN = 15;
    private final int EXTRARUN = 16;
    private final int TOTALRUN = 17;
    private final int DISMISSALKIND = 19;

    public void findMatchesPerYear(List<Match> matches){
        Map<String,Integer> matchesPerYear = new TreeMap<>();
        for(Match match: matches ){
            matchesPerYear.put(match.getSeason(),matchesPerYear.getOrDefault(match.getSeason(),0)+1);
        }
        System.out.println(matchesPerYear);
    }

    public void findMatchesWonPerTeam(List<Match> matches){
        Map<String,Integer> matchesWonPerTeam= new HashMap<>();
        for(Match match : matches ){
            if(!match.getWinnerTeam().equals("")) {
                matchesWonPerTeam.put(match.getWinnerTeam(),matchesWonPerTeam.getOrDefault(match.getWinnerTeam(),0)+1);
            }
        }
        System.out.println(matchesWonPerTeam);
    }

    public void findExtraRunPerTeam2016(List<Match> matches,List<Delivery> deliveries){
        Map<String,Integer> extraRunPerTeam = new HashMap<>();
        for(Match match: matches ) {
            String season = match.getSeason();
            if (season.equals("2016")) {
                int matchId = match.getMatchId();
                for (Delivery delivery : deliveries) {
                    if (matchId == delivery.getMatchId()) {
                        extraRunPerTeam.put(delivery.getBowlingTeam(), extraRunPerTeam.getOrDefault(delivery.getBowlingTeam(), 0) + delivery.getExtraRuns());
                    }
                }
            }
        }
        System.out.println(extraRunPerTeam);
    }

    public void findTopEconomicalBowlers2015(List<Match> matches, List<Delivery> deliveries) {
        Map<String,List<Integer>>  topEconomicalBowler = new HashMap<>();
        Map<String,Float> topEconomyBowler = new HashMap<>();
        for(Match match : matches) {
            if(match.getSeason().equals("2015")){
                for(Delivery delivery : deliveries) {
                    if(match.getMatchId()==delivery.getMatchId()) {
                        List<Integer> bowler =new ArrayList<>();
                        if(topEconomicalBowler.containsKey(delivery.getBowler())){
                            bowler.add(topEconomicalBowler.get(delivery.getBowler()).get(0)+delivery.getTotalRuns());
                            bowler.add(topEconomicalBowler.get(delivery.getBowler()).get(1)+1);
                            topEconomicalBowler.put(delivery.getBowler(),bowler);
                        }
                        else {
                            bowler.add(delivery.getTotalRuns());
                            bowler.add(1);
                            topEconomicalBowler.put(delivery.getBowler(),bowler);
                        }
                    }
                }
            }
        }
        for(Map.Entry<String,List<Integer>> bowlers : topEconomicalBowler.entrySet()){
            List<Integer> runAndBall = new ArrayList<>();
            runAndBall = bowlers.getValue();
            topEconomyBowler.put(bowlers.getKey(),runAndBall.get(0)/(runAndBall.get(1)/6f));
        }
        Set<Map.Entry<String,Float>> entrySet = topEconomyBowler.entrySet();
        List<Map.Entry<String,Float>> bowlerList = new ArrayList<>(entrySet);
        bowlerList.sort(new Comparator<>() {
            @Override
            public int compare(Map.Entry<String, Float> o1, Map.Entry<String, Float> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });
        System.out.println(bowlerList);
    }

    public void findWonTossAndMatch(List<Match> matches) {
        Map<String,Integer> wonTossAndMatch = new HashMap<>();
        for(Match match: matches ) {
            if(match.getTossWinner().equals(match.getWinnerTeam())){
                wonTossAndMatch.put(match.getTossWinner(),wonTossAndMatch.getOrDefault(match.getTossWinner(),0)+1);
            }
        }
        System.out.println(wonTossAndMatch);
    }

    public void findManOfTheMatches(List<Match> matches){
        Map<String,Integer> manOfTheMatches = new HashMap<>();
        for(Match match: matches){
            if(!match.getPlayerOfMatch().equals("")) {
                manOfTheMatches.put(match.getPlayerOfMatch(), manOfTheMatches.getOrDefault(match.getPlayerOfMatch(), 0) + 1);
            }
        }
        System.out.println(manOfTheMatches);
    }

    public void findViratKohliRun2016(List<Match> matches, List<Delivery> deliveries) {
        int run = 0;
        for(Match match : matches) {
            if(match.getSeason().equals("2016")) {
                for(Delivery delivery : deliveries ){
                    if(match.getMatchId() == delivery.getMatchId() && delivery.getBatsman().equals("V Kohli")){
                        run += delivery.getBatsmanRuns();
                    }
                }
            }
        }
        System.out.printf("\nVirat Kohli Run in 2016 \n%d%n", run);
    }

    public void findBumrahOutKohli(List<Delivery> deliveries) {
        int out=0;
        for(Delivery delivery : deliveries ){
            if(delivery.getDismissalKind() != null && delivery.getBowler().equals("JJ Bumrah") && delivery.getBatsman().equals("V Kohli") && !delivery.getDismissalKind().equals("run out")){
                out+=1;
            }
        }
        System.out.println("\nJJ Bumrah Out Virat Kohli "+out+" Times");
    }

    public void findMatchesTieTeamCount(List<Match> matches) {
        Map<String,Integer> teamsMatchesTie = new HashMap<>();
        for(Match match : matches){
            if(match.getMatchResult().equals("tie")) {
                teamsMatchesTie.put(match.getTeam1(),teamsMatchesTie.getOrDefault(match.getTeam1(),0)+1);
                teamsMatchesTie.put(match.getTeam2(),teamsMatchesTie.getOrDefault(match.getTeam2(),0)+1);
            }
        }
        System.out.println(teamsMatchesTie);
    }

    public void findTotalRunsScoreByTeamIn2016(List<Match> matches, List<Delivery> deliveries) {
        Map<String,Integer> totalRunByTeam = new HashMap<>();
        for(Match match : matches) {
            if(match.getSeason().equals("2016")){
                for(Delivery delivery : deliveries) {
                    if(match.getMatchId() == delivery.getMatchId()) {
                        totalRunByTeam.put(delivery.getBattingTeam(),totalRunByTeam.getOrDefault(delivery.getBattingTeam(),0)+1);
                    }
                }
            }
        }
        System.out.println(totalRunByTeam);
    }

    public static void main(String []args) throws IOException {
        Main main = new Main();
        List<Match> matches = new ArrayList<>();
        List<Delivery> deliveries = new ArrayList<>();
        try {
            BufferedReader brDeliveriesData = new BufferedReader(new FileReader("./src/data/deliveries.csv"));
            BufferedReader brMatchesData = new BufferedReader(new FileReader("./src/data/matches.csv"));
            String line;
            brMatchesData.readLine();
            while ((line=brMatchesData.readLine()) != null) {
                String []matchData = line.split(",");
                Match match = new Match();
                match.setMatchId(Integer.parseInt(matchData[main.MATCHID]));
                match.setSeason(matchData[main.SEASON]);
                match.setTeam1(matchData[main.TEAM1]);
                match.setTeam2(matchData[main.TEAM2]);
                match.setTossWinner(matchData[main.TOSSWINNER]);
                match.setMatchResult(matchData[main.RESULT]);
                match.setWinnerTeam(matchData[main.WINNERTEAM]);
                match.setPlayerOfMatch(matchData[main.PLAYEROFMATCH]);
                matches.add(match);
            }
            brDeliveriesData.readLine();
            while ((line=brDeliveriesData.readLine()) != null) {
                String []deliveryData = line.split(",");
                Delivery delivery =new Delivery();
                delivery.setMatchId(Integer.parseInt(deliveryData[main.MATCHID]));
                delivery.setBattingTeam(deliveryData[main.BATTINGTEAM]);
                delivery.setBowlingTeam(deliveryData[main.BOWLINGTEAM]);
                delivery.setBatsman(deliveryData[main.BATSMEN]);
                delivery.setBowler(deliveryData[main.BOWLING]);
                delivery.setBatsmanRuns(Integer.parseInt(deliveryData[main.BATSMENRUN]));
                delivery.setExtraRuns(Integer.parseInt(deliveryData[main.EXTRARUN]));
                delivery.setTotalRuns(Integer.parseInt(deliveryData[main.TOTALRUN]));
                if(deliveryData.length>18) {
                    delivery.setDismissalKind(deliveryData[main.DISMISSALKIND]);
                }
                deliveries.add(delivery);
            }
            System.out.println("Number of matches played per year of all the years in IPL");
            main.findMatchesPerYear(matches);
            System.out.println("\nNumber of matches won of all teams over all the years of IPL.");
            main.findMatchesWonPerTeam(matches);
            System.out.println("\nFor the year 2016 get the extra runs conceded per team.");
            main.findExtraRunPerTeam2016(matches,deliveries);
            System.out.println("\nFor the year 2015 get the top economical bowlers.");
            main.findTopEconomicalBowlers2015(matches,deliveries);
            System.out.println("\nTeam Won the toss and won match also");
            main.findWonTossAndMatch(matches);
            System.out.println("\nNO of man of the matches players won");
            main.findManOfTheMatches(matches);
            main.findViratKohliRun2016(matches,deliveries);
            main.findBumrahOutKohli(deliveries);
            System.out.println("\nNo of time matches tie per team");
            main.findMatchesTieTeamCount(matches);
            System.out.println("\nTotal run Score by a Teams in 2016");
            main.findTotalRunsScoreByTeamIn2016(matches,deliveries);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
